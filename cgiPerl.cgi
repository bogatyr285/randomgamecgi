#!/usr/bin/perl
use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);

print <<eof;
Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Guess A Number</title>
</head>
<body>
eof

warningsToBrowser(1);

$magicNumber = param('magicNumber');
$guess = param('guess');
$attempts = param('attempts');
$salt = param('salt');
$staticSalt=1337;
$gameOver = 0;

if (defined $magicNumber and defined $guess and defined $attempts and defined $salt) {
    $guess =~ s/\D//g;
    $magicNumber =~ s/\D//g;
    $attempts =~ s/\D//g;
    $magicNumber=($magicNumber - $staticSalt)/$salt;
    $attempts=($attempts - $staticSalt)/$salt;  
    $magicNumber =~ m/^\d+.\d+$/;
    if ($magicNumber-int( $magicNumber)!=0 || $attempts-int($attempts)!=0 ){
        print "OMG!!! HACKER ATTACK!111";
        exit;
    }
   print $magicNumber;
    if ($guess>100 || $guess<0){
      print "Incorrect input";          
    }elsif ($guess == $magicNumber) {
        print "U guessed right, it was $magicNumber.\n";
        $gameOver = 1;
    }elsif ($attempts == 1){
    print "U lose(999";
    $gameOver = 1; 
    }elsif ($guess < $magicNumber) {
     $attempts--;
     print "Its higher than $guess. U have ", $attempts, " attemption \n";
    } else {
     $attempts--;
     print "Its lower than $guess. U have ", $attempts, " attemption \n";
    }
    $magicNumber=$magicNumber*$salt+$staticSalt;
     $attempts=$attempts*$salt+$staticSalt;   
} else {
    $magicNumber = 1+int(rand 100);
    print "DEBUG ", $magicNumber; 

    $salt = 1+int(rand 999999);
    $magicNumber = $magicNumber*$salt + $staticSalt; 

    $attempts = 7;
    $attempts = $attempts*$salt + $staticSalt;
    print "I've  thought of number 0..100\n";
}

if ($gameOver) {
print <<eof;
    <form method="POST" action="">
        <input type="submit" value="Play Again">
    </form>
eof
} else {
print <<eof;
    <form method="POST" action="">
        <input type="textfield" name="guess" onkeypress="return event.charCode==13 || event.charCode >= 48 && event.charCode <= 57 && document.getElementsByName('guess')[0].value.length<3" >
        <input type="hidden" name="magicNumber" value="$magicNumber">
     <input type="hidden" name="attempts" value="$attempts">
     <input type="hidden" name="salt" value="$salt">
    </form>
    <hr>
    <br>
 <form method="POST" action="">
        <input type="submit" value="Play Again">
    </form>
eof
}

print <<eof;
</body>
</html>
eof
