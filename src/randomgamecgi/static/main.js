$(document).ready(()=> {
    $("#answer").val('')
  
      $('#sendResult').click(() => {
        var answer = $("#answer").val()
        $("#answer").val('')
        if (answer.length == 0) {
            return
        }
        var dataToSend = {
            "tryguess": answer
        }
        sendAjax(dataToSend)
    });
     $('#newGame').click(() => {
        $('#newGame').hide()
        var dataToSend = {
            "wantgame": true
        }
        sendAjax(dataToSend)
    });
});

function sendAjax(dataToSend) {
    $.ajax({
        url: 'app',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(dataToSend),     
        failure: (errMsg) => {
            alert(errMsg);
        }
    });
}
