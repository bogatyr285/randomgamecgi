package main

import (
	"html/template"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/http/fcgi"
	"strconv"
	"time"
)

const templatefile = "/home/bogatyr285/work/sandbox/randomgamecgi/src/randomgamecgi/static/main.gtpl"

type FastCGIServer struct{}

const TOTAL_TRY = 7

type ServerAnswer struct {
	State     string `json:"state,omitempty"` //must be: less-enter number less than we wishes, more-more, win - win, obosralsya - lose
	RemainTry string `json:"remaintry,omitempty"`
	Err       string
}

type UserSession struct {
	MagicNumber int //what we wished
	TryCount    int
}

var UserList map[string]*UserSession = make(map[string]*UserSession)

func newGame() *UserSession {
	rand.Seed(time.Now().UnixNano())
	return &UserSession{
		TryCount:    0,
		MagicNumber: rand.Intn(100),
	}
}
func ReqParser(userNumStr, key string) (serverAnswer ServerAnswer, err error) {
	if _, exist := UserList[key]; !exist {
		UserList[key] = newGame()
	}
	userNum, err := strconv.Atoi(userNumStr)
	if err != nil {
		return ServerAnswer{Err: "Incorrect input"}, err
	}
	if userNum > UserList[key].MagicNumber {
		UserList[key].TryCount++
		serverAnswer.State = "more"
		remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
		serverAnswer.RemainTry = remaintTry
	} else if userNum < UserList[key].MagicNumber {
		UserList[key].TryCount++
		serverAnswer.State = "less"
		remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
		serverAnswer.RemainTry = remaintTry
	}
	if UserList[key].TryCount == TOTAL_TRY {
		serverAnswer.State = "obosralsya"
		remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
		serverAnswer.RemainTry = remaintTry
		delete(UserList, key)

	} else if UserList[key].MagicNumber == userNum {
		serverAnswer.State = "win"
		tryCount := strconv.Itoa(UserList[key].TryCount + 1)
		serverAnswer.RemainTry = tryCount
		delete(UserList, key)
	}
	log.Print("user list")
	for _, v := range UserList {
		log.Printf("%+v", v)
	}

	return
}
func (s FastCGIServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	serverAnswer := ServerAnswer{}
	var err error

	check(r.ParseForm(), "parsing form")

	key := r.RemoteAddr[:len(r.RemoteAddr)-6]

	tryGuess := r.Form.Get("tryguess")
	log.Printf("receive %v ", tryGuess)
	if tryGuess != "" && len(tryGuess) < 3 {
		serverAnswer, err = ReqParser(tryGuess, key)
		if err != nil {
			log.Printf("ReqParse err %v", err)
		}
	} else {
		serverAnswer.Err = "Incorrect input"
	}

	log.Printf("serv asn:%+v", serverAnswer)

	header := w.Header()
	header.Set("Content-Type", "text/html; charset=utf-8")
	t, err := template.ParseFiles(templatefile)
	check(err, "parsing template")
	err = t.Execute(w, serverAnswer)
	check(err, "executing template")
}

func main() {
	listener, _ := net.Listen("tcp", "127.0.0.1:9001")
	srv := new(FastCGIServer)
	fcgi.Serve(listener, srv)
}
func check(err error, msg string) {
	if err != nil {
		log.Print(msg, err)
	}
}
